using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Vuforia;
using ZXing;
using ZXing.QrCode;

public class QRCodeScanner : MonoBehaviour
{
    public float printedTargetSize;
    public string targetName;

    private Texture2D textureFile;
    private Renderer trackRenderer;
    private bool cameraInitialized;
    private BarcodeReader barCodeReader;
    private List<string> links = new List<string>();

    private void Start()
    {
        barCodeReader = new BarcodeReader();
        StartCoroutine(InitializeCamera());
    }

    private IEnumerator InitializeCamera()
    {
        // Waiting a little seem to avoid the Vuforia's crashes.
        yield return new WaitForSeconds(1.25f);

        var isFrameFormatSet = VuforiaBehaviour.Instance.CameraDevice.SetFrameFormat(PixelFormat.RGB888, true);
        Debug.Log("FormatSet: " + isFrameFormatSet);

        // Force autofocus.
        var isAutoFocus = VuforiaBehaviour.Instance.CameraDevice.SetFocusMode(FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        if (!isAutoFocus)
        {
            VuforiaBehaviour.Instance.CameraDevice.SetFocusMode(FocusMode.FOCUS_MODE_NORMAL);
        }
        Debug.Log("AutoFocus: " + isAutoFocus);
        cameraInitialized = true;
    }

    private void Update()
    {
        if (cameraInitialized)
        {
            try
            {
                var cameraFeed = VuforiaBehaviour.Instance.CameraDevice.GetCameraImage(PixelFormat.RGB888);
                if (cameraFeed == null)
                    return;

                var data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);
                if (data != null)
                {
                    // QRCode detected.
                    Debug.Log(data.Text);

                    if (data.Text.StartsWith(Uri.UriSchemeHttps))
                    {
                        if (!links.Contains(data.Text)) // I should check if I have the file name for photos that I have
                        {
                            links.Add(data.Text);

                            Texture2D myQR = generateQR(data.Text);
                            textureFile = myQR;

                            StartCoroutine(RetrieveTextureFromWeb(data.Text));

                            VuforiaApplication.Instance.OnVuforiaStarted += CreateImageTargetFromSideloadedTexture;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }
        }
    }

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    public Texture2D generateQR(string text)
    {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    private void CreateImageTargetFromSideloadedTexture()
    {
        var mTarget = VuforiaBehaviour.Instance.ObserverFactory.CreateImageTarget(textureFile, printedTargetSize, targetName);

        // add the Default Observer Event Handler to the newly created game object
        mTarget.gameObject.AddComponent<DefaultObserverEventHandler>();

        GameObject newPlan = GameObject.CreatePrimitive(PrimitiveType.Plane);
        newPlan.transform.SetParent(mTarget.transform);
        newPlan.transform.localScale = new Vector3(0.1f, 1, 0.1f);
        newPlan.transform.rotation = Quaternion.Euler(new Vector3(0, -180, 0));
        newPlan.transform.position = new Vector3(0, 0.05f, 0);
        trackRenderer = newPlan.GetComponent<Renderer>();
    }

    private IEnumerator RetrieveTextureFromWeb(string url)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();

            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                // Get downloaded texture once the web request completes
                var texture = DownloadHandlerTexture.GetContent(uwr);
                trackRenderer.material.mainTexture = texture;
            }
        }
    }
}
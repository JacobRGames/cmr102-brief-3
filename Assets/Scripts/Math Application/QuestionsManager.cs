using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestionsManager : MonoBehaviour
{
    [Header("UI Buttons")]
    [SerializeField] private List<ButtonInfo> questionButtons;

    [Header("UI Text")]
    [SerializeField] private TextMeshProUGUI questionText;

    private float lastFirstNum = 999;
    private float lastSecondNum = 999;
    private char lastOperator = ' ';

    [SerializeField] private UnityEvent onCorrectAnswer;
    [SerializeField] private UnityEvent onWrongAnswer;

    private static QuestionsManager instance;
    public static QuestionsManager Instance { get { return instance; } }

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    public void GenerateAnswers(float firstNum, float secondNum, char operatorType)
    {
        if (lastFirstNum == firstNum && lastSecondNum == secondNum && lastOperator == operatorType)
            return;

        ResetAnswers();

        lastFirstNum = firstNum;
        lastSecondNum = secondNum;
        lastOperator = operatorType;

        questionButtons = ShuffleButtons(questionButtons);
        float correctAnswer = Calculator.Calculate(firstNum, secondNum, operatorType.ToString());

        if (operatorType != '/')
            correctAnswer = Mathf.Round(correctAnswer);

        for (int i = 0; i < questionButtons.Count; i++)
        {
            if (i == 0)
            {
                questionButtons[i].button.onClick.AddListener(() => AnswerComplete(true));

                if (operatorType == '/')
                    questionButtons[i].buttonText.text = correctAnswer.ToString("F2");
                else
                    questionButtons[i].buttonText.text = correctAnswer.ToString();
            }
            else
            {
                float tempNum1 = firstNum + Random.Range(-5f, 5f);
                float tempNum2 = firstNum + Random.Range(-5, 5f);

                float calculatorOutput = Calculator.Calculate(tempNum1, tempNum2, operatorType.ToString());

                if (operatorType != '/')
                    calculatorOutput = Mathf.Round(calculatorOutput);

                while (calculatorOutput == correctAnswer)
                {
                    tempNum1 = firstNum + Random.Range(-5f, 5f);
                    tempNum2 = firstNum + Random.Range(-5, 5f);

                    calculatorOutput = Calculator.Calculate(tempNum1, tempNum2, operatorType.ToString());

                    if (operatorType != '/')
                        calculatorOutput = Mathf.Round(calculatorOutput);
                }

                questionButtons[i].button.onClick.AddListener(() => AnswerComplete(false));
                if (operatorType == '/')
                    questionButtons[i].buttonText.text = calculatorOutput.ToString("F2");
                else
                    questionButtons[i].buttonText.text = calculatorOutput.ToString();
            }
            questionButtons[i].button.gameObject.SetActive(true);
        }

        if (operatorType == '/')
            questionText.text = firstNum.ToString() + " " + "�" + " " + secondNum.ToString() + " = ?";
        else
            questionText.text = firstNum.ToString() + " " + operatorType.ToString() + " " + secondNum.ToString() + " = ?";
    }

    public void AnswerComplete(bool correctAnswer)
    {
        if (correctAnswer)
        {
            ResetAnswers();
            onCorrectAnswer?.Invoke();
        }
        else
        {
            onWrongAnswer?.Invoke();
        }
    }

    private void ResetAnswers()
    {
        questionText.text = string.Empty;

        for (int i = 0; i < questionButtons.Count; i++)
        {
            questionButtons[i].button.onClick.RemoveAllListeners();
            questionButtons[i].buttonText.text = string.Empty;
            questionButtons[i].button.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Shuffle the list using the Fisher�Yates algorithm
    /// </summary>
    /// <param name="buttonList"></param>
    /// <returns></returns>
    private List<ButtonInfo> ShuffleButtons(List<ButtonInfo> buttonList)
    {
        System.Random rng = new System.Random();

        for (int x = buttonList.Count - 1; x > 1; x--)
        {
            int y = rng.Next(x);
            ButtonInfo value = buttonList[y];
            buttonList[y] = buttonList[x];
            buttonList[x] = value;
        }

        return buttonList;
    }
}

[System.Serializable]
public class ButtonInfo
{
    public Button button;
    public TextMeshProUGUI buttonText;
}
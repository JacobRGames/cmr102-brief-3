using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSprite : MonoBehaviour
{
    [SerializeField] private float fadeSpeed = 4;
    [SerializeField] private float timeTillFade = 2;
    private Image image;
    private float timeStamp;
    private float currentAlpha = 0;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        ChangeAlphaColour(image, 0);
        
    }

    public void ShowImage()
    {
        CancelInvoke();
        timeStamp = Time.time + timeTillFade;
        currentAlpha = image.color.a;
        ChangeAlphaColour(image, 1.2f);
        StartCoroutine(FadeIn());
    }

    private void ChangeAlphaColour(Image image, float newAlpha)
    {
        newAlpha = Mathf.Clamp(newAlpha, 0, 1.2f);
        image.color = new Color(image.color.r, image.color.g, image.color.b, newAlpha);
    }

    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(timeTillFade);

        Color curColor = image.color;
        while (image.color.a >= 0)
        {
            curColor.a -= fadeSpeed * Time.deltaTime;
            image.color = curColor;
            yield return null;
        }
    }
}

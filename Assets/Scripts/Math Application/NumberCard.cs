using UnityEngine;

public class NumberCard : MonoBehaviour
{
    [Tooltip("This should go from 0 to 9")]
    public int cardNumber;
}
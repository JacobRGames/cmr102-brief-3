﻿using UnityEngine;

public class Calculator : MonoBehaviour
{
    //Return the result of an operation between 2 floats
    public static float Calculate(float x, float y, string op)
    {
        float result = 0.0f;

        switch (op)
        {
            case "+":
                result = x + y;
                break;

            case "-":
                result = x - y;
                break;

            case "*":
                result = x * y;
                break;

            case "/":
                result = x / y;
                break;
        }

        return result;
    }
}
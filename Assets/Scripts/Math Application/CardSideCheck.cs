using UnityEngine;

public class CardSideCheck : MonoBehaviour
{
    [SerializeField] private string cardTag = "Card";
    [SerializeField] private SideTypes sideTypes;
    public NumberCard numberCard { get; private set; }

    private enum SideTypes
    { left, right };

    private Operator op;
    private Collider cardCollider;

    // Start is called before the first frame update
    private void Start()
    {
        op = transform.root.GetComponent<Operator>();
        cardCollider = GetComponent<Collider>();

        if (op != null)
        {
            if (sideTypes == SideTypes.left)
                op.leftSideCheck = this;
            else
                op.rightSideCheck = this;
        }
        else
            Debug.LogError("There is no operator component for: " + name + " to use");
    }

    private void Update()
    {
        if (!cardCollider.enabled)
            numberCard = null;

        if (numberCard != null && !numberCard.isActiveAndEnabled)
            numberCard = null;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag(cardTag))
        {
            numberCard = collider.GetComponent<NumberCard>();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.CompareTag(cardTag))
        {
            numberCard = null;
        }
    }
}
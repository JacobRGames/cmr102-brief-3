using TMPro;
using UnityEngine;

public class Operator : MonoBehaviour
{
    [Tooltip("Example: + | - | x | /")]
    [SerializeField] private char operatorType;

    [HideInInspector] public CardSideCheck leftSideCheck;
    [HideInInspector] public CardSideCheck rightSideCheck;

    // Update is called once per frame
    private void Update()
    {
        if (leftSideCheck?.numberCard != null && rightSideCheck?.numberCard != null)
            QuestionsManager.Instance.GenerateAnswers(leftSideCheck.numberCard.cardNumber, rightSideCheck.numberCard.cardNumber, operatorType);
    }
}
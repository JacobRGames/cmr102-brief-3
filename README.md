# CMR102 - Brief 3

## About the projects
This is my brief 3 project for my augmented reality technologies class.
And in this repository, I have two projects that are utilising Vuforia.

- **The first project:** This project utilises augmented reality to make an experience similar to palm cards when kids are learning English or mathematics. 
But in this case, I am only using it for mathematics, and the way it works is by having 2 number cards and one operator card. 
And when you put them all together in the correct order, it will project the answer to that math question.


- **The second project:** This project is a QR code tracker that scans a QR code and then begins tracking that QR code. 
But not only that, but the QR code scanner will also download the image from the QR code and start projecting it on top of that QR code.

--------------------------------------------------------------

## The math application
**How to use**
 1. Find the folder label “Card Images”.
 2. Grab two number cards and an operator card from this folder.
 3. Press play on Unity.
 4. Put the operator card in the middle of the camera.
 5. Put one of your number cards on each side of the operator card.
 6. The calculated number will pop up on the top of your screen.

**Limitations**
- Sometimes, you need to remove all cards from the camera’s view.
- Try not to have too many cards in the cameras for you at once.

--------------------------------------------------------------

## The QR code tracker
**How to use**
 1. Find the folder labelled “QR Code Images”.
 2. Grab any one of the QR codes.
 3. Press play on Unity.
 4. Put your QR code in front of the camera.
 5. The image from that QR code should pop up shortly.

**Limitations**
- All images linked by a QR code need to be PNG.
- The image tracker might get confused if it says too many QR codes.

--------------------------------------------------------------
